MakeHuman 2 Measure
===================

A MakeHuman avatar custom-tailored to your measurements.

This project proposes a system that takes a set of predefined body measurements,
input by the user, and translates those to Modeling modifier values that
MakeHuman understands. This allows for automatically generating a MakeHuman
avatar that fits the requested body measurements as closely as possible.

This project delivers a machine-learning approach for the measurements to 
MakeHuman translation, delivering a complete toolchain both for training a
knowledge database from a randomly generated sample population for a set of
defined measurements, and using this knowledge base for translating new 
measurements into MakeHuman inputs. This approach delivers a flexible solution
that can be integrated in various projects, allowing users to train a custom
set of measurements that fit their requirements.


License
-------

This project is published under the GPLv3 license, see COPYING for more details.


Usage
-----

This software builds upon the commandline version of MakeHuman, which at the 
time of writing (MakeHuman 1.1.1) lives as a separate feature branch at
https://bitbucket.org/duststorm01/makehuman-commandline but is planned to be
included in a future release of the MakeHuman software.

Currently, you'll have to copy the files in this repository in the
makehuman-commandline/makehuman folder.

Update the file measurements/my_measurements.json to the set of body 
measurements you want to use. The file defines measurements as a map of a
measurement name, and the vertex indices on that MakeHuman basemesh between
which the measurement should be taken.

From the makehuman-commandline/makehuman folder, run 
mh2measure_generate_trainingset.py to generate a set of training data.
