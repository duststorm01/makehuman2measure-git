#!/usr/bin/python2.7

"""
    MakeHuman 2 Measure, from measurements to a MakeHuman avatar
    Copyright (C) 2018  Jonas Hauquier <jonas.hauquier@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


Example how MakeHuman commandline can be used to generate a mesh fitting
specified modifier parameters.

All the modifier values can be specified as commandline arguments, but as there
are quite a lot of such parameters, it's more convenient to store them in a
file.
This example stores the modifier values in a simple JSON dictionary and feeds
them to MH commandline as an illustration.
"""

import time
import sys
import os

MESH_TYPE = 'obj'

try:
    import makehuman
    makehuman.set_sys_path()
    import headless
except ImportError:
    print """Whoops, something went wrong!
I could not import the makehuman commandline code.
Make sure to put this file in the makehuman-commandline/makehuman folder. You can find it at https://bitbucket.org/duststorm01/makehuman-commandline"""
    raise

def output_makehuman_mesh(filename, modifier_values, mesh_type='obj'):
    """This is the most important part of the example. It shows how MH can be
    invoked with a dictionary of modifier values (key=modifier name, 
    value=modifier value) and made to export a mesh."""
    output_filename = os.path.splitext(filename)[0] + '.' + mesh_type

    # Generate default commandline arguments with an explicit output option
    args = ['-o', output_filename]
    args = makehuman.parse_arguments(args)
    if args['modifier'] is None:
       args['modifier'] = []
    # Add our custom modifiers to the arguments
    args['modifier'] = modifier_values

    # Note: Another way instead of passing modifiers, but loading .mhm files currently does not work with commandline
    #args['mhmFile'] = 'path/to/human.mhm'

    # Run MakeHuman with the arguments
    makehuman.get_platform_paths()
    makehuman.redirect_standard_streams()
    makehuman.init_logging()

    from core import G
    G.args = args

    headless.run(args)
    print "Mesh written to %s" % output_filename

    makehuman.close_standard_streams()


def usage():
    print """%s <json_filename>
  Generate a mesh from the modifier values stored in JSON file.
""" % sys.argv[0]

def main():
    print """MakeHuman 2 Measure  Copyright (C) 2018  Jonas Hauquier
    This program comes with ABSOLUTELY NO WARRANTY; for details see
    the file COPYING that came with this software.
    This is free software, and you are welcome to redistribute it
    under certain conditions.\n"""

    if len(sys.argv) != 2:
        print usage()
        sys.exit(-1)

    import json
    filename = sys.argv[1]
    with open(filename, 'r') as f:
        json_data = json.load(f)
    modifier_values = [[modifier_name, str(modifier_value)] for modifier_name, modifier_value in json_data.items()]
    f.close()

    output_makehuman_mesh(os.path.basename(filename), modifier_values, MESH_TYPE)


if __name__ == '__main__':
    main()

