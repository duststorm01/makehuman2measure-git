#!/usr/bin/python2.7

"""
    MakeHuman 2 Measure, from measurements to a MakeHuman avatar
    Copyright (C) 2018  Jonas Hauquier <jonas.hauquier@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import time
import sys
import os

try:
    import makehuman
    makehuman.set_sys_path()
    import headless
except ImportError:
    print """Whoops, something went wrong!
I could not import the makehuman commandline code.
Make sure to put this file in the makehuman-commandline/makehuman folder. You can find it at https://bitbucket.org/duststorm01/makehuman-commandline"""
    raise


from core import G


def setup_app():
    G.app = headless.ConsoleApp()
    human = G.app.selectedHuman


def export_mh_inputs(modifier_dict, mhm_filename):
    """Export modifier values as a .mhm file that can be loaded with MakeHuman
    to generate a mesh from.
    Can be used with makehuman as follows:
        makehuman <filename>.mhm
    Note that makehuman-commandline at this time DOES NOT support loading of
    .mhm files (in non-gui mode).
    """
    f = open(mhm_filename, 'w')

    modifiers = []
    for m_name, m_value in modifier_dict:
        modifiers.append("modifier %s %s" % (m_name, m_value))
    modifiers = "\n".join(modifiers)

    f.write("""# Written by MakeHuman 2 Measure
version v1.1.1
%s
eyes HighPolyEyes 2c12f43b-1303-432c-b7ce-d78346baf2e6
clothesHideFaces True
skinMaterial skins/default.mhmat
material HighPolyEyes 2c12f43b-1303-432c-b7ce-d78346baf2e6 eyes/materials/brown.mhmat
subdivide False
""" % modifiers)

    f.close()

import numpy as np
class Ruler:
    """Ruler defines a set of rulers on the human from which measurements
    can be taken. Vertex indices on the basemesh are used as reference points.
    """

    def __init__(self, filename):
        """Create a set of measurement rulers from a measurement definition file."""
        self.Measures = {}
        self.fromFile(filename)

        self._validate()  # TODO this is only required if it is the intention to draw the rulers on the mesh, not a requirement per se for measuring

    def fromFile(self, filename):
        import json
        from collections import OrderedDict
        data = json.load(open(filename, 'rb'), object_pairs_hook=OrderedDict)
        for measurement_name, ruler_vertices in data.items():
            self.Measures[measurement_name] = ruler_vertices

    def _validate(self):
        """        
        Verify currectness of ruler specification
        """
        names = []
        for n,v in self.Measures.items():
            if len(v) % 2 != 0:
                names.append(n)
        if len(names) > 0:
            raise RuntimeError("One or more measurement rulers contain an uneven number of vertex indices. It's required that they are pairs indicating the begin and end point of every line to draw. Rulers with uneven index count: %s" % ", ".join(names))

    def getMeasure(self, human, measurementname, mode='metric'):
        measure_indices = self.Measures[measurementname]

        vecs = human.meshData.coord[measure_indices[:-1]] - human.meshData.coord[measure_indices[1:]]
        measures = np.sqrt(np.sum(vecs ** 2, axis=-1))[:,None]
        measure = np.sum(measures)

        if mode == 'metric':
            return 10.0 * measure
        else:
            return 10.0 * measure * 0.393700787

#Ruler = headless._load_from_plugin("0_modeling_a_measurement", "Ruler")  # TODO cant because of GUI dependency
ruler = Ruler(measurements_file)

def get_measures(do_print=True):
    result = {}
    for measure in ruler.Measures:
        m = ruler.getMeasure(h, measure)
        result[measure] = m
        if do_print:
            print measure, ruler.getMeasure(h, measure)
    return result

import random
def getRandomValue(minValue, maxValue, middleValue, sigmaFactor = 0.2):
    rangeWidth = float(abs(maxValue - minValue))
    sigma = sigmaFactor * rangeWidth
    randomVal = random.gauss(middleValue, sigma)
    if randomVal < minValue:
        randomVal = minValue + abs(randomVal - minValue)
    elif randomVal > maxValue:
        randomVal = maxValue - abs(randomVal - maxValue)
    return max(minValue, min(randomVal, maxValue))

def _apply_targets_vert_only(human):
    """Fast way of applying targets, only updates vertices  on human, does not sync
    the rest of the geometry"""
    algos3d.resetObj(human.meshData)
    for (targetPath, morphFactor) in human.targetsDetailStack.iteritems():
        algos3d.loadTranslationTarget(human.meshData, targetPath, morphFactor, None, 0, 0)

def _assignModifierValues(human, valuesDict):
    _tmp = human.symmetryModeEnabled
    human.symmetryModeEnabled = False
    for mName, val in valuesDict.items():
        try:
            human.getModifier(mName).setValue(val)
        except:
            pass
    #human.applyAllTargets()
    _apply_targets_vert_only(human)
    human.symmetryModeEnabled = _tmp

import targets
import algos3d
targets_preloaded = False
# TODO currently copied from random plugin, should make this a reusable component to avoid duplication
def randomize(human, symmetry, macro, height, face, body):
    """Generate parameters for a random human by randomizing modifier values.
    Return a dictionary with modifier name as key and modifier value as value.
    """
    global targets_preloaded
    modifierGroups = []
    if macro:
        modifierGroups = modifierGroups + ['macrodetails', 'macrodetails-universal', 'macrodetails-proportions']
    if height:
        modifierGroups = modifierGroups + ['macrodetails-height']
    if face:
        modifierGroups = modifierGroups + ['eyebrows', 'eyes', 'chin', 
                         'forehead', 'head', 'mouth', 'nose', 'neck', 'ears',
                         'cheek']
    if body:
        modifierGroups = modifierGroups + ['pelvis', 'hip', 'armslegs', 'stomach', 'breast', 'buttocks', 'torso']

    # TODO these can be cached, perhaps we get performance gain
    modifiers = []
    for mGroup in modifierGroups:
        modifiers = modifiers + human.getModifiersByGroup(mGroup)
    # Make sure not all modifiers are always set in the same order 
    # (makes it easy to vary dependent modifiers like ethnics)
    random.shuffle(modifiers)

    # Pre-load targets
    targets_loaded = 0
    if not targets_preloaded:
        print "Pre-loading targets"
        targets_to_load = set()
        for group in modifierGroups:
            targets_to_load.update(targets.getTargets().findTargets(group))
        for target in targets_to_load:
            algos3d.getTarget(human.meshData, target.path)
            targets_loaded += 1
            if targets_loaded % 10 == 0:
                print "loaded %s/%s targets" % (targets_loaded, len(targets_to_load))
        targets_preloaded = True
        print "\n\nRandomizing %s modifiers\n\n" % len(modifiers)
        print '\n'.join(sorted([m.fullName for m in modifiers]))

    randomValues = {}
    for m in modifiers:
        if m.fullName not in randomValues:
            randomValue = None
            if m.groupName == 'head':
                sigma = 0.1
            elif m.fullName in ["forehead/forehead-nubian-less|more", "forehead/forehead-scale-vert-less|more"]:
                sigma = 0.02
                # TODO add further restrictions on gender-dependent targets like pregnant and breast
            elif "trans-horiz" in m.fullName or m.fullName == "hip/hip-trans-in|out":
                if symmetry == 1:
                    randomValue = m.getDefaultValue()
                else:
                    mMin = m.getMin()
                    mMax = m.getMax()
                    w = float(abs(mMax - mMin) * (1 - symmetry))
                    mMin = max(mMin, m.getDefaultValue() - w/2)
                    mMax = min(mMax, m.getDefaultValue() + w/2)
                    randomValue = getRandomValue(mMin, mMax, m.getDefaultValue(), 0.1)
            elif m.groupName in ["forehead", "eyebrows", "neck", "eyes", "nose", "ears", "chin", "cheek", "mouth"]:
                sigma = 0.1
            elif m.groupName == 'macrodetails':
                # TODO perhaps assign uniform random values to macro modifiers?
                #randomValue = random.random()
                sigma = 0.3
            #elif m.groupName == "armslegs":
            #    sigma = 0.1
            else:
                #sigma = 0.2
                sigma = 0.1

            if randomValue is None:
                randomValue = getRandomValue(m.getMin(), m.getMax(), m.getDefaultValue(), sigma)   # TODO also allow it to continue from current value?
            randomValues[m.fullName] = randomValue
            symm = m.getSymmetricOpposite()
            if symm and symm not in randomValues:
                if symmetry == 1:
                    randomValues[symm] = randomValue
                else:
                    m2 = human.getModifier(symm)
                    symmDeviation = float((1-symmetry) * abs(m2.getMax() - m2.getMin()))/2
                    symMin =  max(m2.getMin(), min(randomValue - (symmDeviation), m2.getMax()))
                    symMax =  max(m2.getMin(), min(randomValue + (symmDeviation), m2.getMax()))
                    randomValues[symm] = getRandomValue(symMin, symMax, randomValue, sigma)

    if randomValues.get("macrodetails/Gender", 0) > 0.5 or \
       randomValues.get("macrodetails/Age", 0.5) < 0.2 or \
       randomValues.get("macrodetails/Age", 0.7) < 0.75:
        # No pregnancy for male, too young or too old subjects
        if "stomach/stomach-pregnant-decr|incr" in randomValues:
            randomValues["stomach/stomach-pregnant-decr|incr"] = 0

    _assignModifierValues(h, randomValues)
    return randomValues

db_file = open("mh_measure_db.csv", 'w')
keys = None
measurement_keys = None
def store(modifier_values, measurements):
    global keys
    global measurement_keys
    if keys is None:
        keys = modifier_values.keys()
        measurement_keys = measurements.keys()
        fields = ";".join(keys + ["_measure_"+k for k in measurement_keys])
        db_file.write(fields)
        db_file.write("\r\n")
    fields = []
    for key in keys:
        try:
            fields.append(str(modifier_values[key]))
        except KeyError:
            fields.append("-100")
    for key in measurement_keys:
        try:
            fields.append(str(measurements[key]))
        except KeyError:
            fields.append("-100")
    db_file.write(";".join(fields))
    db_file.write("\r\n")


def generate_training_data():
    start_time = time.time()
    # Randomize a first time to pre-load all targets, dont include this in timing
    modifiers = randomize(h, symmetry=0.7, macro=True, height=True, body=True, face=False)
    end_time = time.time()
    print("\n\nLoading targets took %s seconds\n\n" % (end_time - start_time))

    start_time = time.time()

    N_MODELS = 10000
    for i in range(N_MODELS):
        measures = get_measures(False)
        store(modifiers, measures)
        #print ("====")
        if i %  100 == 0:
            print "%s/%s" % (i, N_MODELS)
        modifiers = randomize(h, symmetry=0.7, macro=True, height=True, body=True, face=False)

    measures = get_measures()

    store(modifiers, measures)

    end_time = time.time()
    print("\n\nMeasuring %s random models took %s seconds\n\n" % (N_MODELS, end_time - start_time))

    db_file.close()

def generate_training_data():
    h = G.app.selectedHuman
    m = h.mesh

    import humanargparser
    humanargparser._selectivelyLoadModifiers(h)

    import getpath
    measurement_data_paths = [ getpath.getPath('measurements'),
                               getpath.getSysDataPath('measurements') ]
    measurements_file = getpath.thoroughFindFile("my_measurements.json", measurement_data_paths)

def main():
    print """MakeHuman 2 Measure  Copyright (C) 2018  Jonas Hauquier
    This program comes with ABSOLUTELY NO WARRANTY; for details see
    the file COPYING that came with this software.
    This is free software, and you are welcome to redistribute it
    under certain conditions."""

    setup_app()
    generate_training_data()


if __name__ == '__main__':
    main()

